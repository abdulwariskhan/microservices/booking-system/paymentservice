package com.microservice.paymentservice.utils;

public enum PaymentMode {
    CREDIT_CARD,
    DEBIT_CARD,
    CASH
}
